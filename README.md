# jsproject


[💡 Image Pro Tip: Paste an image into a GitHub issue comment (any repo). It will give back the markdown you can paste above. You don't even need to submit it!]

[Give a more detailed explanation of what your package does here. A few sentences or a few paragraphs.
This is your elevator pitch area.]

## Installation

```bash
$ npm i jsproject
```

or

```bash
$ yarn add jsproject
```

## Usage

run test files with:

```bash
$ yarn test
```


### Parameters

### Return


## Example


## Live demo

## License

**[ISC](LICENSE)** Licensed

---

