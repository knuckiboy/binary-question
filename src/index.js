var Node = require("./node.js");

function setNextNode(root, node) {
  if (root == null) {
    return null;
  }

  let q = [];
  q.push(root);

  let front;

  while (q.length != 0) {
    let size = q.length;

    while (size-- > 0) {
      front = q.shift();

      if (front == node) {
        if (size == 0) {
          node.next = null;
          break;
        }

        node.next = q[0];
        break;
      }

      if (front.left != null) {
        q.push(front.left);
      }

      if (front.right != null) {
        q.push(front.right);
      }
    }
  }
}

function createTree( arr, n) {
  let root = null;
  let q =[]

  function insertNode(value) {
    let node = new Node(value);
    if (root == null) root = node;
    else if (q[0].left == null) {
      q[0].left = node;
    } else {
      q[0].right = node;
      q.shift();
    }
    q.push(node);
  }

  for (let i = 0; i < n; i++) {
    insertNode(arr[i]);
  }
  return root;
}


function addNextNode(root) {
  if (root == null) return;
  
  let n = [];
  n.push(root);
  
  while (n.length > 0) {
    setNextNode(root, n[0]);
    if (n[0].left != null) n.push(n[0].left);
    if (n[0].right != null) n.push(n[0].right);
    
    n.shift();
  }
}


function createSimpleResultArr(root, nodeArr) {
  if (root == null) return;
  
  let n = [];
  n.push(root);
  
  while (n.length > 0) {
    if (n[0].left != null) n.push(n[0].left);
    if (n[0].right != null) n.push(n[0].right);
    if (n[0].val != null) {
      nodeArr.push(n[0].val);
    }
    n.shift();
  }
}

function createResultArr(root, nodeArr) {
  if (root == null) return;
  
  let n = [];
  n.push(root);
  
  while (n.length > 0) {
    if (n[0].left != null) n.push(n[0].left);
    if (n[0].right != null) n.push(n[0].right);
    if (n[0].val != null) {
      nodeArr.push(n[0].val);
    }
    if (n[0].next == null) {
      nodeArr.push("#");
    }
    n.shift();
  }
}

module.exports = {createTree, addNextNode, createSimpleResultArr,createResultArr}