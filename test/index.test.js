var  assert = require("assert");

var { createTree, addNextNode, createSimpleResultArr, createResultArr } = require("../src/index.js");

let sample = [1, 2, 3, 4, 5, null, 7];

describe("create binary tree", function () {
  it("should be able to create binary tree from arr", function () {
    let root = createTree(sample,sample.length);
    let nodeArr = [];
    createSimpleResultArr(root,nodeArr)
    assert.deepEqual(nodeArr,[1,2,3,4,5,7], "not strictly equal arr")
    
  });
});

describe("create binary with next node in tree item", function () {
    it("should be able to create binary tree of nodes with next nodes from arr", function () {
      let root = createTree(sample,sample.length);
      let nodeArr = [];
      addNextNode(root)
      createResultArr(root,nodeArr)
      assert.deepEqual(nodeArr,[1,'#',2,3,'#',4,5,7,'#'], "not strictly equal arr")
      
    });
  });
